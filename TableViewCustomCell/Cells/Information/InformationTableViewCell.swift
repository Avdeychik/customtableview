//
//  InformationTableViewCell.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class InformationTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "InformationTableViewCell"
    
    weak var cellModel: InformationTableViewCellModel?
    
    let textInformationLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        configureInformationLabel()
    }
    
    func configureInformationLabel() {
        addSubview(textInformationLabel)
        textInformationLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textInformationLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 30),
            textInformationLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            textInformationLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -90),
            textInformationLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -30),
        ])
    }
}

extension InformationTableViewCell: FillableCell {
    
    func fill(cellModel: CellModel) {
        guard let cellModel = cellModel as? InformationTableViewCellModel else { return }
        self.cellModel = cellModel
        textInformationLabel.text = cellModel.textInformation
    }
}
