//
//  InformationTableViewCellModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import Foundation

class InformationTableViewCellModel: CellModel {
    
    let textInformation: String
    
    init(textInformation: String) {
        self.textInformation = textInformation
        super.init(cellIdentifier: InformationTableViewCell.cellIdentifier)
    }
}
