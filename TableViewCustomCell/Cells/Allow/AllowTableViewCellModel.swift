//
//  AllowTableViewCellModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 15.11.21.
//

import Foundation

class AllowTableViewCellModel: CellModel {
    
    let textAllow: String
    
    init(headerText: String) {
        self.textAllow = headerText
        super.init(cellIdentifier: AllowTableViewCell.cellIdentifier)
    }
}
