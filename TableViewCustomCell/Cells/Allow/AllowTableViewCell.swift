//
//  AllowTableViewCell.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class AllowTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "AllowTableViewCell"
    
    weak var cellModel: AllowTableViewCellModel?
    
    let textAllowLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 33, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configure()
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        configureAllowLabel()
    }
    
    func configureAllowLabel() {
        contentView.addSubview(textAllowLabel)
        textAllowLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textAllowLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 80),
            textAllowLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 30),
            textAllowLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -50),
            textAllowLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
        ])
    }
}

extension AllowTableViewCell: FillableCell {
    
    func fill(cellModel: CellModel) {
        guard let cellModel = cellModel as? AllowTableViewCellModel else { return }
        self.cellModel = cellModel
        textAllowLabel.text = cellModel.textAllow
    }
}
