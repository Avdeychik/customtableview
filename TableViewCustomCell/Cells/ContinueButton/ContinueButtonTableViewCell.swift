//
//  ContinueButtonTableViewCell.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import UIKit

class ContinueButtonTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "ContinueButtonTableViewCell"
    
    weak var cellModel: ContinueButtonTableViewCellModel?
    
    var continueButton: UIButton = {
        let button = UIButton(type: .system)
        let buttonColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        button.setTitle("Continue", for: .normal)
        button.setTitleColor(buttonColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        button.backgroundColor = .white
        button.layer.cornerRadius = 35
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        configureContinueButton()
    }
    
    func configureContinueButton() {
        contentView.addSubview(continueButton)
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            continueButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
            continueButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            continueButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
            continueButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30),
            continueButton.heightAnchor.constraint(equalToConstant: 70)
        ])
    }
}

extension ContinueButtonTableViewCell: FillableCell {
    
    func fill(cellModel: CellModel) {
        guard let cellModel = cellModel as? ContinueButtonTableViewCellModel else { return }
        self.cellModel = cellModel
    }
}
