//
//  ContinueButtonTableViewCellModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import Foundation

class ContinueButtonTableViewCellModel: CellModel {
    
    init() {
        super.init(cellIdentifier: ContinueButtonTableViewCell.cellIdentifier)
    }
}
