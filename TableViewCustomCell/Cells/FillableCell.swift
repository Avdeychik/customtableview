//
//  FillableCell.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 15.11.21.
//

import Foundation

protocol FillableCell {
    
    func fill(cellModel: CellModel)
}
