//
//  AllowsFeatureTableViewCell.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import UIKit

class AllowsFeatureTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "AllowsFeatureTableViewCell"
    
    weak var cellModel: AllowsFeatureTableViewCellModel?
    
    let imageAllowFeature: UIImageView = {
        let image = UIImageView()
        image.image = UIImage()
        image.tintColor = .white
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let textAllowFeatureLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureViews() {
        configureImageAllowFeature()
        configureAllowFeatureLabel()
    }
    
    private func configureImageAllowFeature() {
        imageAllowFeature.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageAllowFeature)
        NSLayoutConstraint.activate([
            imageAllowFeature.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 30),
            imageAllowFeature.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            imageAllowFeature.heightAnchor.constraint(equalToConstant: 50),
            imageAllowFeature.widthAnchor.constraint(equalToConstant: 50)
        ])
    }

    private func configureAllowFeatureLabel() {
        textAllowFeatureLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(textAllowFeatureLabel)
        NSLayoutConstraint.activate([
            textAllowFeatureLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            textAllowFeatureLabel.leftAnchor.constraint(equalTo: imageAllowFeature.rightAnchor, constant: 16),
            textAllowFeatureLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -50),
            textAllowFeatureLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        ])
    }
}

extension AllowsFeatureTableViewCell: FillableCell {
    
    func fill(cellModel: CellModel) {
        guard let cellModel = cellModel as? AllowsFeatureTableViewCellModel else { return }
        self.cellModel = cellModel
        imageAllowFeature.image = UIImage(systemName: cellModel.imageAllowFeature.rawValue)
        textAllowFeatureLabel.text = cellModel.textAllowFeature
    }
}
