//
//  AllowsFeatureTableViewCellModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import Foundation

class AllowsFeatureTableViewCellModel: CellModel {
    
    let imageAllowFeature: AllowsFeatureIcons
    let textAllowFeature: String
    
    init(imageAllowFeature: AllowsFeatureIcons, textAllowFeature: String) {
        self.imageAllowFeature = imageAllowFeature
        self.textAllowFeature = textAllowFeature
        super.init(cellIdentifier: AllowsFeatureTableViewCell.cellIdentifier)
    }
}
