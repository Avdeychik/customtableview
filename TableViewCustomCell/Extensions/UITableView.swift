//
//  UITableView.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import UIKit

extension UITableView {
    func register(typeInString: String) {
        guard let type = NSClassFromString("TableViewCustomCell.\(typeInString)") else { return }
        self.register(type.self, forCellReuseIdentifier: typeInString)
    }
}
