//
//  DataSource.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 15.11.21.
//

import Foundation

protocol DataSource {
    
    var input: ScreenModel { get }
}
