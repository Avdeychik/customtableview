//
//  GreenViewController.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class GreenViewController: UIViewController {
    
    let model = GreenModel()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.bounces = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        view.backgroundColor = .systemGreen
        
        tableView.backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        tableView.dataSource = model.dataSource
        
        tableView.separatorStyle = .none
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
}












//extension GreenViewController: {
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//            guard input.cellModels.count > indexPath.row else { return .init() }
//            let cellModel = input.cellModels[indexPath.row]
//            tableView.register(cellModel.cellIdentifier)
//
//            // FIXME: - Need change color
//            let cell = tableView.dequeueReusableCell(
//                withIdentifier: cellModel.cellIdentifier,
//                for: indexPath
//            )
//            if let fillableCell = cell as? FillableCell {
//                fillableCell.fill(cellModel: cellModel)
//            }
//            return cell
//        }
//}
