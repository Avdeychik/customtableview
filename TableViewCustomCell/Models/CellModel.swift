//
//  CellModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 15.11.21.
//

import Foundation

class CellModel {
    
    let cellIdentifier: String
    
    init(cellIdentifier: String) {
        self.cellIdentifier = cellIdentifier
    }
}
