//
//  ScreenModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 15.11.21.
//

import Foundation

protocol ScreenModel: AnyObject {
    
    var cellModels: [CellModel] { get }
}
