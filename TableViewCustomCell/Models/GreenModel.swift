//
//  GreenModel.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class GreenModel: ScreenModel {
    
    lazy var dataSource = GreenViewDataSource(input: self)
    
    private(set) var cellModels: [CellModel] = [
        AllowTableViewCellModel(headerText: "Allow traking on the next screen for:"),
        
        AllowsFeatureTableViewCellModel(imageAllowFeature: AllowsFeatureIcons.heart,
                                        textAllowFeature: "Special offers and promotions just for you"),
        AllowsFeatureTableViewCellModel(imageAllowFeature: AllowsFeatureIcons.handPoint,
                                        textAllowFeature: "Advertisements that match your interests"),
        AllowsFeatureTableViewCellModel(imageAllowFeature: AllowsFeatureIcons.chartBar,
                                        textAllowFeature: "An improved personalized experience over time"),
        
        InformationTableViewCellModel(textInformation: "You can change this option later in the Settings app."),
        
        ContinueButtonTableViewCellModel()
    ]
}
