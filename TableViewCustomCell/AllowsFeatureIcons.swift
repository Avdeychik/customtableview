//
//  AllowsFeatureIcons.swift
//  TableViewCustomCell
//
//  Created by Алексей Авдейчик on 16.11.21.
//

import Foundation

enum AllowsFeatureIcons: String {
    
    case heart = "heart.fill"
    case handPoint = "hand.point.up.braille.fill"
    case chartBar = "chart.bar.fill"
}
